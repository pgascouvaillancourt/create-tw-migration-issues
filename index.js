require("dotenv").config();

const axios = require("axios");
const { startCase, capitalize } = require("lodash");
const cssInJs = require(process.env.PATH_TO_CSS_IN_JS);

const COLLECTIONS = {
  text: /text-/,
  fontSize: /font-size/,
  backgroundColor: /bg-/,
  display: /display-/,
  boxShadow: /(inset-border|shadow)-/,
  border: /(border|rounded)/,
  margin: /(gl-|-)m[xytrbl]?-n?\d/,
  padding: /(gl-|-)p[xytrbl]?-n?\d/,
  flex: /(flex|align-items|justify-content|align-self)-/,
  opacity: /opacity-/,
  transition: /transition-/,
  lineHeight: /line-height-/,
  fontWeight: /font-weight-/,
  sizing: /(gl-|-)(max-|min-)?[hw]-/,
  zIndex: /z-index-/,
  verticalAlign: /vertical-align-/,
  transform: /translate-/,
  whiteSpace: /white-space-/,
  heading: /heading-/,
  font: /font-/,
  visibility: /visibility-/,
  wordBreak: /word-break-/,
  lineClamp: /line-clamp-/,
  overflowWrap: /overflow-wrap-/,
  listStyle: /list-style-/,
  outline: /outline-/,
  columnGap: /column-gap-/,
  rowGap: /row-gap-/,
};

const LINKS = {
  text: [
    "https://tailwindcss.com/docs/text-color",
    "https://tailwindcss.com/docs/text-overflow",
    "https://tailwindcss.com/docs/text-decoration",
    "https://tailwindcss.com/docs/text-align",
  ],
  fontSize: ["https://tailwindcss.com/docs/font-size"],
  backgroundColor: ["https://tailwindcss.com/docs/background-color"],
  display: ["https://tailwindcss.com/docs/display"],
  boxShadow: ["https://tailwindcss.com/docs/box-shadow"],
  border: [
    "https://tailwindcss.com/docs/border-width",
    "https://tailwindcss.com/docs/border-color",
    "https://tailwindcss.com/docs/border-style",
    "https://tailwindcss.com/docs/border-radius",
  ],
  margin: ["https://tailwindcss.com/docs/margin"],
  padding: ["https://tailwindcss.com/docs/padding"],
  flex: [
    "https://tailwindcss.com/docs/flex",
    "https://tailwindcss.com/docs/flex-basis",
    "https://tailwindcss.com/docs/flex-direction",
    "https://tailwindcss.com/docs/flex-wrap",
    "https://tailwindcss.com/docs/flex-grow",
    "https://tailwindcss.com/docs/justify-content",
    "https://tailwindcss.com/docs/align-items",
    "https://tailwindcss.com/docs/align-self",
  ],
  opacity: ["https://tailwindcss.com/docs/opacity"],
  transition: [
    "https://tailwindcss.com/docs/transition-property",
    "https://tailwindcss.com/docs/transition-duration",
    "https://tailwindcss.com/docs/transition-timing-function",
  ],
  lineHeight: ["https://tailwindcss.com/docs/line-height"],
  fontWeight: ["https://tailwindcss.com/docs/font-weight"],
  sizing: [
    "https://tailwindcss.com/docs/width",
    "https://tailwindcss.com/docs/min-width",
    "https://tailwindcss.com/docs/max-width",
    "https://tailwindcss.com/docs/height",
    "https://tailwindcss.com/docs/min-height",
    "https://tailwindcss.com/docs/max-height",
    "https://tailwindcss.com/docs/size",
  ],
  zIndex: ["https://tailwindcss.com/docs/z-index"],
  verticalAlign: ["https://tailwindcss.com/docs/vertical-align"],
  transform: ["https://tailwindcss.com/docs/translate"],
  whiteSpace: ["https://tailwindcss.com/docs/whitespace"],
  heading: [],
  font: ["https://tailwindcss.com/docs/font-style"],
  visibility: ["https://tailwindcss.com/docs/visibility"],
  wordBreak: ["https://tailwindcss.com/docs/word-break"],
  lineClamp: ["https://tailwindcss.com/docs/line-clamp"],
  overflowWrap: ["https://tailwindcss.com/docs/word-break"],
  listStyle: ["https://tailwindcss.com/docs/list-style-type"],
  outline: ["https://tailwindcss.com/docs/outline-width"],
  columnGap: ["https://tailwindcss.com/docs/gap"],
  rowGap: ["https://tailwindcss.com/docs/gap"],
};

const importantUtilities = [];
const statefulUtilities = [];
const responsiveAndImportantUtilities = [];
const statefulAndImportantUtilities = [];
const responsiveUtilities = [];
const normalUtilities = [];

const buckets = {
  importantUtilities: {},
  statefulUtilities: {},
  responsiveAndImportantUtilities: {},
  statefulAndImportantUtilities: {},
  responsiveUtilities: {},
  normalUtilities: {},
};

const isImportant = (utilName) => utilName.endsWith("!");
const isResponsive = (utilName) => utilName.match(/^\.(sm|md|lg)-/);
const isStateful = (utilName) =>
  utilName.match(/^\.(hover|focus|active|visited)-/);
const isResponsiveAndImportant = (utilName) =>
  isResponsive(utilName) && isImportant(utilName);
const isStatefulAndImportant = (utilName) =>
  isStateful(utilName) && isImportant(utilName);

const getCleanUtilName = (utilName) =>
  utilName.replace(/\\+!$/, "!").replace(/^\./, ".gl-");
const getHumanizedName = (name) => capitalize(startCase(name));

Object.entries(cssInJs).forEach(([key, value]) => {
  if (isResponsiveAndImportant(key)) {
    responsiveAndImportantUtilities.push(getCleanUtilName(key));
  } else if (isStatefulAndImportant(key)) {
    statefulAndImportantUtilities.push(getCleanUtilName(key));
  } else if (isImportant(key)) {
    importantUtilities.push(getCleanUtilName(key));
  } else if (isStateful(key)) {
    statefulUtilities.push(getCleanUtilName(key));
  } else if (isResponsive(key)) {
    responsiveUtilities.push(getCleanUtilName(key));
  } else {
    normalUtilities.push(getCleanUtilName(key));
  }
});

const makeBuckets = ([bucketName, utils]) => {
  utils.forEach((util) => {
    const isInSomeBucket = Object.entries(COLLECTIONS).some(
      ([collectionName, pattern]) => {
        if (pattern.test(util)) {
          buckets[bucketName][collectionName] =
            buckets[bucketName][collectionName] || [];
          buckets[bucketName][collectionName].push(util);
          return true;
        }
        return false;
      }
    );
    if (!isInSomeBucket) {
      buckets[bucketName].miscellaneous =
        buckets[bucketName].miscellaneous || [];
      buckets[bucketName].miscellaneous.push(util);
    }
  });
};

[
  ["responsiveAndImportantUtilities", responsiveAndImportantUtilities],
  ["statefulAndImportantUtilities", statefulAndImportantUtilities],
  ["importantUtilities", importantUtilities],
  ["statefulUtilities", statefulUtilities],
  ["responsiveUtilities", responsiveUtilities],
  ["normalUtilities", normalUtilities],
].forEach(makeBuckets);

const importantUtilsMigrationStep =
  "Move the `!` to the start of the util (eg `gl-bg-blue-100!` becomes `!gl-bg-blue-100`).";
const statefulUtilsMigrationStep =
  "Convert the pseudo-selector to a valid Tailwind prefix (eg `gl-hover-border-gray-200` becomes `hover:gl-border-gray-200`).";
const responsiveUtilsMigrationStep =
  "Convert the breakpoint fragment to a valid Tailwind prefix (eg `gl-sm-text-body` becomes `sm:gl-text-body`).";

let index = 0;
Object.entries(buckets).forEach(([bucketName, bucketContent]) => {
  let humanizedBucketName = getHumanizedName(bucketName);
  if (humanizedBucketName === "Normal utilities") {
    humanizedBucketName = "";
  } else {
    humanizedBucketName += " > ";
  }
  Object.entries(bucketContent).forEach(([collectionName, utils]) => {
    let humanizedCollectionName = getHumanizedName(collectionName);
    if (humanizedCollectionName === "Z index") {
      humanizedCollectionName = "z-index";
    }
    const migrationSteps = [
      /important/i.test(bucketName) && importantUtilsMigrationStep,
      /stateful/i.test(bucketName) && statefulUtilsMigrationStep,
      /responsive/i.test(bucketName) && responsiveUtilsMigrationStep,
      !/(important|stateful|responsive)/i.test(bucketName) &&
        "If this issue has important/stateful/responsive siblings and they are simple enough, consider migrating them all at once.",
      "Ensure the util aligns with Tailwind's naming conventions",
      "Migrate all usages of the util to the Tailwind equivalent. It might be used in HAML, Ruby, Javascript and Vue files. This might warrant several MRs.",
      "If our Tailwind configuration doesn't seem to support a replacement, we might need to add the missing values to GitLab's `tailwind.config.js`. In doubt, reach out to the task group in [`#tg_css_utils`](https://gitlab.enterprise.slack.com/archives/C05CPKD5GTD) (internal).",
      "If you did add new values to the configuration, please add a `TODO:` along with it if it needs to be backported to GitLab UI eventually.",
    ].filter(Boolean);

    const links = LINKS[collectionName] || [];
    const linksSection = links.length
      ? `### Links

You may refer to the following Tailwind CSS documentation pages. Those should help in figuring out
the naming convention the utils should follow. Keep in mind that we use a custom preset, meaning that
our Tailwind equivalents should follow the same structure as what is outlined in Tailwind docs, but
the actual names and applied values might differ. For example, all our utilities use the \`gl-\` prefix,
we use a custom color palette and spacing scale, etc.

${links.map((link) => `- ${link}`).join("\n")}
`
      : "";

    const issueTitle = `[CSS utilities TG] ${humanizedBucketName}${humanizedCollectionName} > Migrate to Tailwind CSS`;
    const issueDescription = `
The following utilities need to be migrated to Tailwind CSS:

${utils.map((util) => `- [ ] \`${util}\``).join("\n")}

### Migration steps

${migrationSteps.map((step) => `- ${step}`).join("\n")}

${linksSection}
`;
    setTimeout(async () => {
      try {
        const {
          data: { web_url },
        } = await axios.post(
          `${process.env.GITLAB_INSTANCE_URL}/api/v4/projects/${process.env.PROJECT_ID}/issues`,
          {
            title: issueTitle,
            description: issueDescription,
            epic_iid: process.env.EPIC_IID,
            labels: ["frontend", "maintenance::refactor", "workflow::blocked"],
          },
          {
            headers: {
              Authorization: `Bearer ${process.env.API_TOKEN}`,
            },
          }
        );
        console.log(`${issueTitle} created: ${web_url}`);
      } catch (e) {
        console.log(e);
      }
    }, index * 1000);
    index += 1;
  });
});
