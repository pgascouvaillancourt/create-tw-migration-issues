# Create Tailwind CSS migration issues

## Running against the GDK

1. Make sure your GDK is up-to-date and you have built the CSS-in-Js:
    ```sh
    yarn pretailwindcss:build
    ```
1. Run your GDK.
1. Create the `.env` file, set the variables to the relevant values.
    ```sh
    cp .env.example .env
    ```
1. Install the dependencies
    ```sh
    yarn
    ```
1. Run the script
    ```sh
    node ./index.js
    ```